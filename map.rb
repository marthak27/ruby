# new_arr = (1..5).to_a.map {|element| element * 2}
# puts new_arr.inspect

new_arr = ['apple', 'banana', 'cherry'].map.with_index do |element, index|
    "Element #{index + 1}: #{element}"
end
# Element 1:apple, ...
puts new_arr.inspect